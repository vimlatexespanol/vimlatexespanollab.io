# Nueva Página. Con nuevas novedades™️

Como podrán ver, ahora esta página tiene un dominio web nuevo. Y está alojada. Así que la verán dando vueltas por ahí por lo menos durante **un año**.

Por primera vez tengo la oportunidad de subir una página personal, pero que la estoy personalizando propiamente para darle difusión a lo relacionado con mi [canal de youtube](https://www.youtube.com/c/vimylatexenespañol).

Ahora bien, de lo que estoy seguro es que bajo el dominio de [https://vimlatexesp.xyz] voy a estar haciendo diferentes páginas sencillas, además de algunos artículos relacionados con software libre y otras cosas.

Les pido que estén al pendiente.

Saludos.
