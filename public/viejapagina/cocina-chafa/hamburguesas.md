# Hamburguesas

Una receta simple para hacer unas cuantas hamburguesas.

## Ingredientes:

- 1 Kg de Carne molida de res o de cerdo. Al gusto.
- Una bolsa de pan molido (opcional)
- Bollos para pan.
- Esto es una prueba

# Preparación

1. Amasamos la carne molida con sal, pimienta y pan molido al gusto. La función del pan molido es de darle uniformidad a nuestras tortas de carne (la carne de nuestras hamburguesas).
1. Podemos dejarlas envueltas en papel encerado o plástico para cuando sea el momento de freir nuestra carne.
1. Freímos en aceite hirviendo hasta que la carne quede en el término que queremos.
1. Etcétera.
