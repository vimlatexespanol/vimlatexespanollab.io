# Donaciones

Si quieres colaborar con un servidor, primeramente agradeceré mucho la suscripción a mi canal, y los me gusta. Estos permiten que mi contenido sea recomendado a más usuarios.

Asímismo, puedes ver mis videos desde LBRY u Odysee, y de esta manera también estarás haciéndome llegar tu apoyo

También puedes hacerlo vía cripto:

| Criptomoneda | Adress                                                                                            | QR                       |
| Monero       | `8BaNvwttj481N3enndjzoRRoKDeW9RTqY8Zf4cCBqFLdXw8a4KtA64aDrb45WCkeMK72i8dXLkpdeZqnZFPQWuHmM3AsmmE` | ![Monero](qr/monero.png) |

